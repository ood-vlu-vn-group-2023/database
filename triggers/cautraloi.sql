CREATE TRIGGER before_insert_cautraloi
BEFORE INSERT ON public.cautraloi
FOR EACH ROW
EXECUTE FUNCTION limit_cautraloi();

CREATE TRIGGER after_insert_cautraloi
AFTER INSERT ON public.cautraloi
FOR EACH ROW
EXECUTE FUNCTION delete_logcautraloi();

CREATE TRIGGER after_delete_cautraloi
AFTER DELETE ON public.cautraloi
FOR EACH ROW
EXECUTE FUNCTION insert_logcautraloi();