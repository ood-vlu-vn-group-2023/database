CREATE TRIGGER after_insert_quyen
AFTER INSERT ON public.quyen
FOR EACH ROW
EXECUTE FUNCTION delete_logquyen();

CREATE TRIGGER after_delete_quyen
AFTER DELETE ON public.quyen
FOR EACH ROW
EXECUTE FUNCTION insert_logquyen();