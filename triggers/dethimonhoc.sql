CREATE TRIGGER before_insert_dethimonhoc
BEFORE INSERT ON public.dethimonhoc
FOR EACH ROW
EXECUTE FUNCTION prevent_update_if_thoigiandethichinh_depend();

CREATE TRIGGER after_insert_dethimonhoc
AFTER INSERT ON public.dethimonhoc
FOR EACH ROW
EXECUTE FUNCTION delete_logdethimonhoc();

CREATE TRIGGER after_delete_dethimonhoc
AFTER DELETE ON public.dethimonhoc
FOR EACH ROW
EXECUTE FUNCTION insert_logdethimonhoc();