CREATE TRIGGER before_update_cauhoidethi
BEFORE UPDATE ON CauHoiDeThi
FOR EACH ROW
EXECUTE FUNCTION prevent_update_cauhoidethi();

CREATE TRIGGER before_delete_cauhoidethi
BEFORE DELETE ON CauHoiDeThi
FOR EACH ROW
EXECUTE FUNCTION prevent_delete_cauhoidethi();