CREATE TRIGGER before_insert_thoigiandethichinh
BEFORE INSERT ON public.thoigiandethichinh
FOR EACH ROW
EXECUTE FUNCTION insert_thoigiandethichinh_if_dethimonhocreal();

CREATE TRIGGER before_update_thoigiandethichinh
BEFORE UPDATE ON public.thoigiandethichinh
FOR EACH ROW
EXECUTE FUNCTION update_thoigiandethichinh_if_dethimonhocreal();

CREATE TRIGGER after_insert_thoigiandethichinh
AFTER INSERT ON public.thoigiandethichinh
FOR EACH ROW
EXECUTE FUNCTION delete_logthoigiandethichinh();

CREATE TRIGGER after_delete_thoigiandethichinh
AFTER DELETE ON public.thoigiandethichinh
FOR EACH ROW
EXECUTE FUNCTION insert_logthoigiandethichinh();