CREATE TRIGGER before_update_cauhoi
BEFORE UPDATE ON public.cauhoi
FOR EACH ROW
EXECUTE FUNCTION prevent_if_cautraloi_not_in_cauhoi();

CREATE TRIGGER after_insert_cauhoi
AFTER INSERT ON public.cauhoi
FOR EACH ROW
EXECUTE FUNCTION delete_logcauhoi();

CREATE TRIGGER after_delete_cauhoi
AFTER DELETE ON public.cauhoi
FOR EACH ROW
EXECUTE FUNCTION insert_logcauhoi();