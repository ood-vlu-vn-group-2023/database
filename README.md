# database

Database sử dụng PostgreSQL

## Line of Code (LOC: Lượng dòng code)

Sử dụng phần mềm [cloc](https://github.com/AlDanial/cloc) để tính toán 

+ Không tính insert dữ liệu:
```sh
      50 text files.
      50 unique files.
       0 files ignored.

github.com/AlDanial/cloc v 1.98  T=0.02 s (2984.9 files/s, 224284.4 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
SQL                             50            327            259           3171
-------------------------------------------------------------------------------
SUM:                            50            327            259           3171
-------------------------------------------------------------------------------
```

+ Tính luôn insert dữ liệu và những file khác:
```sh
      53 text files.
      53 unique files.
       0 files ignored.

github.com/AlDanial/cloc v 1.98  T=0.02 s (2955.8 files/s, 246612.2 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
SQL                             53            425            279           3718
-------------------------------------------------------------------------------
SUM:                            53            425            279           3718
-------------------------------------------------------------------------------
```
