/* Insert */
CREATE OR REPLACE PROCEDURE insert_nguoidung(
    p_account     TEXT,
    p_quyen_id    INT,
    p_password    TEXT,
    p_displayname TEXT
) AS
$$
BEGIN
    INSERT INTO NguoiDung (nguoidung_account, quyen_id, nguoidung_password, nguoidung_displayname)
    VALUES (p_account, p_quyen_id, p_password, p_displayname);
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_nguoidung(
    p_account     TEXT,
    p_quyen_id    INT,
    p_password    TEXT,
    p_displayname TEXT
) AS
$$
BEGIN
    UPDATE nguoidung
    SET
        quyen_id = p_quyen_id,
        nguoidung_password = p_password,
        nguoidung_displayname = p_displayname
    WHERE nguoidung_account = p_account;
END;
$$
LANGUAGE plpgsql;

/* Cập nhập người dùng mà không có quyen_id */
CREATE OR REPLACE PROCEDURE update_nguoidung_without_quyen(
    p_account     TEXT,
    p_password    TEXT,
    p_displayname TEXT
) AS
$$
BEGIN
    UPDATE nguoidung
    SET
        nguoidung_password = p_password,
        nguoidung_displayname = p_displayname
    WHERE nguoidung_account = p_account;
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_nguoidung(p_account TEXT)
AS
$$
BEGIN
    DELETE FROM nguoidung
    WHERE nguoidung_account = p_account;
END;
$$
LANGUAGE plpgsql;