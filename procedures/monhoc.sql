/* Insert */
CREATE OR REPLACE PROCEDURE insert_monhoc(
    p_lophoc_id INT,
    p_name      TEXT
) AS
$$
DECLARE
    log_count  INT;
    log_array  INT[] := ARRAY[]::INT[];
    value_last INT;
    item       INT;
BEGIN
    -- Lấy tổng giá trị trong bảng log.monhoc
    SELECT COUNT(log.monhoc.monhoc_id) INTO log_count FROM log.monhoc;

    -- Nếu tổng giá trị trong bảng log.monhoc lớn hơn thì thêm với id từ bảng
    IF (log_count > 0) THEN
        log_array := ARRAY (
            SELECT log.monhoc.public_monhoc_id FROM log.monhoc
                ORDER BY log.monhoc.public_monhoc_id ASC LIMIT 1
        );

        item := log_array[1];

        -- Thêm dữ liệu
        INSERT INTO monhoc (monhoc_id, lophoc_id, monhoc_name)
            VALUES (item, p_lophoc_id, p_name);
    ELSE
        -- Lấy vị trí cuối
        SELECT LAST_VALUE(monhoc_id) OVER (ORDER BY monhoc_id DESC)
            INTO value_last FROM monhoc;

        -- Trường hợp thêm
        IF (value_last >= 1) THEN
            INSERT INTO monhoc (monhoc_id, lophoc_id, monhoc_name)
                VALUES (value_last+1, p_lophoc_id, p_name);
        ELSE
            INSERT INTO monhoc (monhoc_id, lophoc_id, monhoc_name)
                VALUES (1, p_lophoc_id, p_name);
        END IF;
    END IF;
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_monhoc(
    p_id        INT,
    p_lophoc_id INT,
    p_name      TEXT
) AS
$$
BEGIN
    UPDATE monhoc
    SET
        lophoc_id = p_lophoc_id,
        monhoc_name = p_name
    WHERE monhoc_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_monhoc(p_id INT)
AS
$$
BEGIN
    DELETE FROM monhoc
    WHERE monhoc_id = p_id;
END;
$$
LANGUAGE plpgsql;